import json

def getSettings():
	try:
		with open("appsettings.json") as file:
			return json.load(file)
	except:
		exit("Erreur dans le chargement du paramétrage.")

def getNameToken(settings):
	tokensList = tuple(settings["tokens"].keys())
	if len(tokensList) == 1:
		return tokensList[0]

	print("----- Liste des tokens disponibles -----")
	for token_name in tokensList:
		print(token_name)

	select = None
	while select not in tokensList:
		select = input("> Quel token choisir ? ")
	return select

def printNomJoueurs(liste):
	for j in liste:
		print(j["name"])

def demanderInt(phrase):
	print(phrase)
	while True:
		rep = input()
		try:
			return int(rep)
		except:
			print("Réponse invalide. Veuillez entrer un nombre.")

def demanderBool(phrase, ok):
	rep = input(phrase)
	return (rep in ok)
