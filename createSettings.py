from utils import demanderBool
import json
import os.path

def getTokens():
    res = {}
    while True:
        name = input("Quel est le nom pour votre token ? (Vide pour finir)\n>")
        if name == "":
            return res
        token = input("Quel est votre token ?\n>")
        res[name] = token

def getListName():
    res = []
    while True:
        name = input("Quels pseudos épargnez-vous ? (Vide pour finir)\n>")
        if name == "":
            return res
        res.append(name)

def main():
    if os.path.isfile("appsettings.json") and \
        not demanderBool("Le fichier de paramétrage existe déjà. Voulez-vous le réinitialiser ? (O/o) ", ('O', 'o')):
            return
    with open("appsettings2.json", "w") as file:
        settings = {}
        settings["codeJoueur"] = input("Quel est votre identifiant de joueur ? ")
        settings["codeClan"] = input("Quel est l'identifiant de votre clan ? ")
        settings["tokens"] = getTokens()
        settings["intouchables"] = getListName()
        json.dump(settings, file)

if __name__ == '__main__':
	main()
