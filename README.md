# Création d'un logiciel d'aide à la gestion de clan sur Clash Royal.
Programme de gestion de clan et de statistique de personnage sur Clash Royal.
Ce logiciel repose sur l'[api officiel du jeu](https://developer.clashroyale.com/#/).
Les fonctionnalités actuelles sont les suivantes :
1. Affichage des prochains coffres
2. Affichage des moins actifs du clan (en don)
3. Affichage des moins actifs du clan (en guerre)
4. Affichage des plus actifs du clan (en don)
5. Affichage des plus actifs du clan (en guerre)

## Pré-requis
* Nécessite un token de connexion créable [ici](https://developer.clashroyale.com/#/).
Attention cette clé dépend de votre adresse IP !
* Utilisation d'un interpréteur Python3.6 minimum.

## Lancement de l'application
Avant de lancer une première fois le programme, vous devez configurer le
paramétrage de l'application. Pour cela exécutez la commande suivante `python3 createSettings.py`

Ensuite, l'application se lance avec la commande `python3 main.py`
