from connexion import getConnexion
from utils import *
from fctJoueurs import *

def printMenu():
	print(f"{'='*20} MENU {'='*20}")
	print("  1. Affichage des prochains coffres")
	print("  2. Affichage des moins actifs (en don)")
	print("  3. Affichage des moins actifs (en guerre)")
	print("  4. Affichage des plus actifs (en don)")
	print("  5. Affichage des plus actifs (en guerre)")
	print("="*46)

def actifDon(joueurs):
	minDon = demanderInt("Quel minimum pour définir les plus actifs ? ")
	actifs = grandDon(joueurs, minDon)
	print(f"----- Plus de {minDon} dons -----")
	printNomJoueurs(actifs)

def actifGuerre(participants):
	minPart = demanderInt("Quel minimum pour définir les plus actifs en guerre ? ")
	grandeParticipation(participants, minPart)

def nonActifDon(joueurs, settings):
	maxDon = demanderInt("Quel maximum pour définir les nons actifs ? ")
	ok = demanderBool("Protéger les intouchables ? [O/n]", ('O','o'))
	inactifs = zeroDon(joueurs, maxDon,
		settings["intouchables"] if ok else ())
	print(f"----- Moins de {maxDon} dons -----")
	printNomJoueurs(inactifs)

def nonActifGuerre(participants, joueurs, settings):
	maxPart = demanderInt("Quel maximum pour définir les non actifs en guerre ? ")
	ok = demanderBool("Protéger les intouchables ? [O/n]", ('O','o'))
	zeroParticipation(participants, joueurs, maxPart,
		settings["intouchables"] if ok else ())

def main():
	settings = getSettings()
	connexion = getConnexion(settings)

	joueurs = getJoueurs(connexion)
	participants = getParticipations(connexion)

	while True:
		printMenu()
		rep = input("Que faire ? Appuyer sur q pour quitter.\n>")
		if rep == '1':
			printChest(connexion)
		elif rep == '2':
			nonActifDon(joueurs, settings)
		elif rep == '3':
			nonActifGuerre(participants, joueurs, settings)
		elif rep == '4':
			actifDon(joueurs)
		elif rep == '5':
			actifGuerre(participants)
		elif rep == 'q':
			return
		else:
			print("Erreur : entrée invalide.")


if __name__ == '__main__':
	main()
