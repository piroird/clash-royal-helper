from connexion import getJson

def getJoueurs(connexion):
	json = getJson("/members", connexion)
	return json["items"]

def zeroDon(listJ, minDon, intouchable):
	return [joueur for joueur in listJ
		if joueur["donations"] <= minDon
			and joueur["name"] not in intouchable]

def grandDon(listJ, minimum):
	return [joueur for joueur in listJ if joueur["donations"] > minimum]

def getParticipations(connexion):
	json = getJson("/warlog", connexion)
	json2 = getJson("/currentwar", connexion)
	if json2["state"] != "notInWar" :
		json["items"].append(json2)

	res = {}
	for war in json["items"]:
		for pers in war["participants"]:
			if pers["name"] not in res:
				res[pers["name"]] = {"nbWar" : 1, "wins" : pers["wins"],
									 "battlesPlayed" : pers["battlesPlayed"],
									 "numberOfBattles" : pers["numberOfBattles"],
									 "collectionDayBattlesPlayed" : pers["collectionDayBattlesPlayed"]}
			else:
				res[pers["name"]]["nbWar"] += 1
				res[pers["name"]]["battlesPlayed"] += pers["battlesPlayed"]
				res[pers["name"]]["numberOfBattles"] += pers["numberOfBattles"]
				res[pers["name"]]["collectionDayBattlesPlayed"] += pers["collectionDayBattlesPlayed"]
				res[pers["name"]]["wins"] += pers["wins"]
	return res

def zeroParticipation(participations, listJ, maximum, intouchable):
	print("----- Non actifs en guerre -----")
	for player in listJ: # Ajout des 0 Participations
		if player["name"] not in participations.keys():
			print(f"{player['name']} - 0 guerre")
	for player in sorted(participations.keys(), key=lambda x: participations[x]["nbWar"]):
		if participations[player]["nbWar"] > maximum:
			break;
		if player not in intouchable:
			print(f"{player} - {participations[player]['nbWar']} guerres")

def grandeParticipation(participations, minimum):
	print("----- Actifs en guerre -----")
	for player in sorted(participations.keys(), key=lambda x: -participations[x]["nbWar"]):
		if participations[player]["nbWar"] < minimum:
			break;
		print(f"{player} : {participations[player]['nbWar']} guerres")

def printChest(connexion):
	json = getJson("/upcomingchests", connexion, False)
	print("----- Liste des prochains coffres -----")
	for el in json["items"]:
		print(f"{el['index']} - {el['name']}")
