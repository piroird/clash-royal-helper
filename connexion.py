import requests
from utils import getNameToken

def getUrls(settings):
	url = "https://api.clashroyale.com/v1/"
	idClan = settings["codeClan"]
	idPers = settings["codeJoueur"]
	return {"clan" : f"{url}clans/%23{idClan[1:]}",
			"players" : f"{url}players/%23{idPers[1:]}"}

def getConnexion(settings):
	token_name = getNameToken(settings)
	token = {'authorization': f"Bearer {settings['tokens'][token_name]}"}
	return {"urls" : getUrls(settings), "header" : token}

def getJson(suffixeUrl, connexion, clan=True):
	url = connexion["urls"]["clan" if clan else "players"]+suffixeUrl
	json = requests.get(url, headers=connexion["header"]).json()
	if "reason" not in json:
		return json
	exit(json["reason"])
